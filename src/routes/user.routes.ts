import {Router} from 'express'
const router = Router();

import {getUser, getUserid, deleteUser, updateUser, InserUser } from '../controllers/user.controller'

router.post('/user', InserUser)
router.get('/user', getUser)
router.get('/user/:id', getUserid)
router.put('/user/:id', updateUser)
router.delete('/user/:id', deleteUser)

export default router;