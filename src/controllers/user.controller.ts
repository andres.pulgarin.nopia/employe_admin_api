import {Request, Response} from 'express'
import employee, {IUser} from '../models/user'
import jwt from 'jsonwebtoken'
import config from '../config/config'

async function createEmail(firstName: string, firstSurname: string, country: string) {
    let pais = country
    let dominio = country === 'Colombia ' ? "@cidenet.com.co" : "@cidenet.com";
    let templateEmail = firstName + firstSurname+dominio
    let email =  await employee.findOne({email: templateEmail})
    let id = 0
    while(email){
        templateEmail = firstName + firstSurname+ id++ +dominio
        email = await employee.findOne({email: templateEmail})
    }
    return templateEmail

}
export const InserUser = async (req: Request , res: Response ): Promise<Response> =>{
    let employeeData = req.body;
    employeeData.email = await createEmail(employeeData.fistName, employeeData.firstSurname, employeeData.country);
    employeeData.regidterDate = new Date();
    employeeData.state = "Activo";
    var User = await employee.findOne({email: employeeData.email})// 
    const idNumber = await employee.findOne({idNumber: employeeData.idNumber})
    if(idNumber){
        
        return res.status(400).json({msg: 'The user already existe'})
    }
    console.log("employeeData: ", employeeData)
    
    
    const newUser = new employee(employeeData);

    await newUser.save();

    return res.status(201).json({msg: 'Usuario Registrado'})
}

export const getUser = async (req: Request , res: Response ) =>{
    
    const nowUser = await employee.find()
    
    return res.status(200).json(nowUser)
}

export const getUserid = async (req: Request , res: Response ) =>{
    const nowUser = await employee.findById(req.params.id)
    
    return res.status(200).json(nowUser)
}

export const updateUser = async (req: Request , res: Response ) =>{
    const updatedTask = await employee.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });
      res.json(updatedTask);
}

export const deleteUser = async (req: Request , res: Response ) =>{
    const deleteUsers = await employee.findByIdAndDelete(req.params.id);
  res.json(deleteUsers);
}