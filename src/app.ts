import express from "express";
import morgan from "morgan";
import passportMiddleware from "./middelwares/passport";
import passport from "passport";
import cors from "cors";
import user from './routes/user.routes'
// initializations
const app = express();

// settings
app.set('port', process.env.PORT || 3200);

app.use(cors());
// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


// routes
app.get('/', (req, res) =>{
    res.send(`THE API is at http://localhost:${app.get('port')}`)
});

app.use(user);
export default app;


