import {model, Schema, Document} from "mongoose";
import bcrypt from 'bcrypt'

export interface IUser extends Document{
    fistName: string;
    otherName: string;
    firstSurname: string;
    secondSurname: string;
    idType: string;
    idNumber: string;
    country: string;
    email: string;
    dateIn: string;
    area: string;
    state: string;
    regidterDate: string; 

    createEmail : (email: string) => Promise<boolean>
}

const employeeSchema = new Schema ({
    email: {
        type: String,
        unique: true,
        requiered: true,
        lowercase: true,
        trim: true
    },
    fistName: {
        type: String,
        required: true,
    },
    otherName: {
        type: String,
        required: false
    },
    firstSurname: {
        type: String,
        required: true
    },
    secondSurname: {
        type: String,
        required: true
    },
    idType: {
        type: String,
        required: true
    },
    idNumber: {
        type: String,
        required: true,
        unique: true
    },
    country: {
        type: String,
        required: true
    },
    dateIn: {
        type: String,
        required: true
    },
    area: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    regidterDate: {
        type: String,
        required: true
    }
});

export default model<IUser>('employee', employeeSchema)